package zhide.wexin.controller;

import io.jsonwebtoken.Claims;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import zhide.wexin.bean.User;
import zhide.wexin.test.SendWxMessage;
import zhide.wexin.utils.HttpRequest;
import zhide.wexin.utils.HttpsUrlValidator;
import zhide.wexin.utils.JWTUtils;
import zhide.wexin.utils.JsonData;


@Controller
public class UserController {


//    @Autowired
//    private RedisUtil redisUtil;

    @Autowired
    private SendWxMessage sendWxMessage;

    @SuppressWarnings({"unchecked", "rawtypes"})//忽略警告
    @RequestMapping(value = "/wxLogin", method = RequestMethod.POST)
    @ResponseBody
    public JsonData decodeUserInfo(String token, @RequestBody(required = false) User user, String code) throws JSONException {


        if (token == null || token.length() == 0 || StringUtils.isEmpty(token)) {


            // 登录凭证不能为空
            if (code == null || code.length() == 0 || StringUtils.isEmpty(code)) {
                ;
                return JsonData.buildError(-1, "code不能为空");

            }

            // 小程序唯一标识 (在微信小程序管理后台获取)
            String wxspAppid = "wxcf35cb2e964097e2";
            // 小程序的 app secret (在微信小程序管理后台获取)
            String wxspSecret = "1b6398baa2cfbfc684ce71f932281611";
            // 授权（必填）
            String grant_type = "authorization_code";

            //1、向微信服务器 使用登录凭证 code 获取 session_key 和 openid

            // 请求参数
            String params = "appid=" + wxspAppid + "&secret=" + wxspSecret + "&js_code=" + code + "&grant_type="
                    + grant_type;
            // 发送请求
            String url  = "https://api.weixin.qq.com/sns/jscode2session";
           /* try {*/
                //先调用下忽略https证书的再请求才可以
                HttpsUrlValidator.retrieveResponseFromServer(url);
           /*     doc = Jsoup
                        .connect(url)
                        .header("User-Agent",rand_agents)
                        .timeout(10000).get();
                body = doc.getElementsByTag("body").html();
            } catch (Exception e) {
                log.info(e.getMessage());
            }

            */
            String sr = HttpRequest.sendGet(url, params);
            // 解析相应内容（转换成json对象）
            JSONObject json = new JSONObject(sr);
            // 获取会话密钥（session_key）
            String session_key = json.get("session_key").toString();
            // 用户的唯一标识（openid）
            String openid = (String) json.get("openid");

            System.out.println(session_key);
            System.out.println(openid);

           /* user.setOpenId(openid);
            user.setSessionKey(session_key);


            String token1 = JWTUtils.geneJsonWebToken(user);*/


            //放入缓存
            //redisUtil.set(openid,token,1000);//将用户的唯一标志openid座位键，token作为值放进redis,设置过期时间

            String accessToken = sendWxMessage.getAccessToken();
            System.out.println(accessToken);

            String oneUser = sendWxMessage.pushOneUser();

            System.out.println(oneUser);


            return JsonData.buildSuccess(openid);
        }
        else {
            return checkToken(token, code);
        }


    }


    public JsonData checkToken(String token, String code) {


        Claims claims = JWTUtils.checkJWT(token);
        //if (StringUtils.isEmpty(claims)) {//不等于空的意思
        if (claims == null || StringUtils.isEmpty(claims)) {
            //告诉登录失败或过期
            return JsonData.buildError(-1, "登录过期，重新登录");

        }
        else {
            System.out.println(claims.get("openId"));
            if (claims.get("openId") == null) {

                return JsonData.buildError("没有此用户信息");
            }
//                claims.get("name");
//                claims.get("sessionKey");
//                claims.get("heam_img");
            return JsonData.buildSuccess(token);//放行

        }


    }

}
